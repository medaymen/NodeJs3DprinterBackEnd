var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// set up a mongoose model and pass it using module.exports
module.exports = mongoose.model('feedback', new Schema({ 
    message: String, 
     user: {
         type: Schema.Types.ObjectId,
         ref:'User'
     }
    
}));