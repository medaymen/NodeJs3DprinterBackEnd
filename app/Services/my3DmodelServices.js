var my3Dmodel = require('../models/3Dmodel');
var ObjectId = require('mongoose').Types.ObjectId;
var User   = require('../models/user')
var multer = require('multer');
var StlThumbnailer = require('node-stl-thumbnailer');
var fs = require('fs');

module.exports =  {

UploadSTLFile :  async (req,res)=>{
 
    uploadSTL(req,res,async(err)=>{

     var thumbnailer =  new StlThumbnailer({
          // url: req.file,           // url OR filePath must be supplied, but not both 
           filePath: "./STLFiles/"+req.file.filename,            // load file from filesystem 
           requestThumbnails: [
               {
                   width: 500,
                   height: 500,
               }
           ]   
       }).then((thumbnails)=>{
        let name = makeid()+".png";
        thumbnails[0].toBuffer( async(err, buf)=>{      
                     fs.writeFileSync("./uploads/"+ name, buf);
                     let user = await User.findById(req.params.userid) ; 

                   let mymodel = new my3Dmodel({
                       stlfilename:req.file.filename,
                       name:name,
                       imagename:req.body.imagename,
                       category:req.body.category,
                       visibility:req.body.visibility, 
                       User:user});
                    await mymodel.save(); 
                   await user.models.push(mymodel);
                   await user.save();
            
                   let modelsofuser = await User.findById(req.params.userid).populate('models') ; 
               
                    res.status(200).json(modelsofuser.models);
                     


              });
        
       
         });
         

       });
       
   
   


},


getMy3Dcollection: async (req,res)=>{


    let modelsofuser = await User.findById(req.params.iduser).populate('models') ; 
               
    res.status(200).json(modelsofuser.models);
},

DeleteModel : async (req,res)=>{
  
     await  my3Dmodel.findOneAndRemove({stlfilename:req.params.modelname});
    fs.unlinkSync('./STLFiles/'+req.params.modelname);
    let modelsofuser = await User.findById(req.params.iduser).populate('models') ; 
               
      res.status(200).json(modelsofuser.models);
  
    
},
GetAll3DModels: async(req,res)=>{

    let my3dmodels = await my3Dmodel.find({visibility:'shared'})
    res.status(200).json(my3dmodels);

} ,
AddToMyCollection : async( req,res)=>{
   let user = await User.findById(req.params.iduser); 
   let model =  await my3Dmodel.findById(req.params.idmodel); 
   user.models.push(model);
   await    user.save();
   res.status(200).json({status:true}) ; 

}  



}













      //==========uploading STLfile
var storage2 = multer.diskStorage({ //multers disk storage settings
    destination: function (req, file, cb) {
        cb(null, './STLFiles/');
    },
    filename: function (req, file, cb) {
        var datetimestamp = Date.now();
        cb(null, file.fieldname + '-' + datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length -1]);
    }
  });
  
  var uploadSTL = multer({ //multer settings
                storage: storage2
            }).single('file');
  //============================
  //======end file upload config 
  //============================
  function makeid() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  
    for (var i = 0; i < 10; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));
  
    return text;
  }